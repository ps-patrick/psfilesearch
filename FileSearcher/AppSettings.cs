﻿using System;
using System.Configuration;
using System.Text;

namespace FileSearcher
{
    public class AppSettings
    {
        private static AppSettings _instance;

        public static AppSettings Instance
        {
            get { return _instance ?? (_instance = new AppSettings()); }
        }
        public AppSettings()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["PSConnectionString"].ConnectionString;
        }

        public string ConnectionString { get; set; }
    }
}
