﻿namespace FileSearcher
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.searchDirTextBox = new System.Windows.Forms.TextBox();
            this.selectSearchDirButton = new System.Windows.Forms.Button();
            this.includeSubDirsCheckBox = new System.Windows.Forms.CheckBox();
            this.newerThanCheckBox = new System.Windows.Forms.CheckBox();
            this.newerThanDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.olderThanDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.olderThanCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.unicodeRadioButton = new System.Windows.Forms.RadioButton();
            this.asciiRadioButton = new System.Windows.Forms.RadioButton();
            this.containingTextBox = new System.Windows.Forms.TextBox();
            this.containingCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnMapToDB = new System.Windows.Forms.Button();
            this.delimeterTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.writeResultsButton = new System.Windows.Forms.Button();
            this.resultsList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openContainingFolderContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMapToDatabase = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.txtCurrentResourceKey = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboPSprojectType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAnotherSearch = new System.Windows.Forms.Button();
            this.ResourceMatchResults = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileNameTextBox.Location = new System.Drawing.Point(273, 122);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(780, 20);
            this.fileNameTextBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Filename (may include wildcards, not case sensitive):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Search directory:";
            // 
            // searchDirTextBox
            // 
            this.searchDirTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchDirTextBox.Location = new System.Drawing.Point(117, 12);
            this.searchDirTextBox.Name = "searchDirTextBox";
            this.searchDirTextBox.Size = new System.Drawing.Size(150, 20);
            this.searchDirTextBox.TabIndex = 1;
            // 
            // selectSearchDirButton
            // 
            this.selectSearchDirButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectSearchDirButton.Location = new System.Drawing.Point(273, 12);
            this.selectSearchDirButton.Name = "selectSearchDirButton";
            this.selectSearchDirButton.Size = new System.Drawing.Size(24, 21);
            this.selectSearchDirButton.TabIndex = 2;
            this.selectSearchDirButton.Text = "...";
            this.selectSearchDirButton.UseVisualStyleBackColor = true;
            this.selectSearchDirButton.Click += new System.EventHandler(this.selectSearchDirButton_Click);
            // 
            // includeSubDirsCheckBox
            // 
            this.includeSubDirsCheckBox.AutoSize = true;
            this.includeSubDirsCheckBox.Location = new System.Drawing.Point(117, 99);
            this.includeSubDirsCheckBox.Name = "includeSubDirsCheckBox";
            this.includeSubDirsCheckBox.Size = new System.Drawing.Size(129, 17);
            this.includeSubDirsCheckBox.TabIndex = 3;
            this.includeSubDirsCheckBox.Text = "Include subdirectories";
            this.includeSubDirsCheckBox.UseVisualStyleBackColor = true;
            // 
            // newerThanCheckBox
            // 
            this.newerThanCheckBox.AutoSize = true;
            this.newerThanCheckBox.Location = new System.Drawing.Point(6, 22);
            this.newerThanCheckBox.Name = "newerThanCheckBox";
            this.newerThanCheckBox.Size = new System.Drawing.Size(106, 17);
            this.newerThanCheckBox.TabIndex = 0;
            this.newerThanCheckBox.Text = "Files newer than:";
            this.newerThanCheckBox.UseVisualStyleBackColor = true;
            this.newerThanCheckBox.CheckedChanged += new System.EventHandler(this.newerThanCheckBox_CheckedChanged);
            // 
            // newerThanDateTimePicker
            // 
            this.newerThanDateTimePicker.CustomFormat = "dd.MM.yyyy HH:mm";
            this.newerThanDateTimePicker.Enabled = false;
            this.newerThanDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.newerThanDateTimePicker.Location = new System.Drawing.Point(261, 19);
            this.newerThanDateTimePicker.Name = "newerThanDateTimePicker";
            this.newerThanDateTimePicker.Size = new System.Drawing.Size(123, 20);
            this.newerThanDateTimePicker.TabIndex = 1;
            // 
            // olderThanDateTimePicker
            // 
            this.olderThanDateTimePicker.CustomFormat = "dd.MM.yyyy HH:mm";
            this.olderThanDateTimePicker.Enabled = false;
            this.olderThanDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.olderThanDateTimePicker.Location = new System.Drawing.Point(261, 45);
            this.olderThanDateTimePicker.Name = "olderThanDateTimePicker";
            this.olderThanDateTimePicker.Size = new System.Drawing.Size(123, 20);
            this.olderThanDateTimePicker.TabIndex = 3;
            // 
            // olderThanCheckBox
            // 
            this.olderThanCheckBox.AutoSize = true;
            this.olderThanCheckBox.Location = new System.Drawing.Point(6, 48);
            this.olderThanCheckBox.Name = "olderThanCheckBox";
            this.olderThanCheckBox.Size = new System.Drawing.Size(100, 17);
            this.olderThanCheckBox.TabIndex = 2;
            this.olderThanCheckBox.Text = "Files older than:";
            this.olderThanCheckBox.UseVisualStyleBackColor = true;
            this.olderThanCheckBox.CheckedChanged += new System.EventHandler(this.olderThanCheckBox_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.unicodeRadioButton);
            this.groupBox1.Controls.Add(this.asciiRadioButton);
            this.groupBox1.Controls.Add(this.containingTextBox);
            this.groupBox1.Controls.Add(this.containingCheckBox);
            this.groupBox1.Controls.Add(this.olderThanDateTimePicker);
            this.groupBox1.Controls.Add(this.newerThanCheckBox);
            this.groupBox1.Controls.Add(this.olderThanCheckBox);
            this.groupBox1.Controls.Add(this.newerThanDateTimePicker);
            this.groupBox1.Location = new System.Drawing.Point(12, 148);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1047, 120);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Restrictions";
            // 
            // unicodeRadioButton
            // 
            this.unicodeRadioButton.AutoSize = true;
            this.unicodeRadioButton.Enabled = false;
            this.unicodeRadioButton.Location = new System.Drawing.Point(319, 97);
            this.unicodeRadioButton.Name = "unicodeRadioButton";
            this.unicodeRadioButton.Size = new System.Drawing.Size(65, 17);
            this.unicodeRadioButton.TabIndex = 7;
            this.unicodeRadioButton.TabStop = true;
            this.unicodeRadioButton.Text = "Unicode";
            this.unicodeRadioButton.UseVisualStyleBackColor = true;
            // 
            // asciiRadioButton
            // 
            this.asciiRadioButton.AutoSize = true;
            this.asciiRadioButton.Enabled = false;
            this.asciiRadioButton.Location = new System.Drawing.Point(261, 97);
            this.asciiRadioButton.Name = "asciiRadioButton";
            this.asciiRadioButton.Size = new System.Drawing.Size(52, 17);
            this.asciiRadioButton.TabIndex = 6;
            this.asciiRadioButton.TabStop = true;
            this.asciiRadioButton.Text = "ASCII";
            this.asciiRadioButton.UseVisualStyleBackColor = true;
            // 
            // containingTextBox
            // 
            this.containingTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.containingTextBox.Enabled = false;
            this.containingTextBox.Location = new System.Drawing.Point(261, 71);
            this.containingTextBox.Name = "containingTextBox";
            this.containingTextBox.Size = new System.Drawing.Size(780, 20);
            this.containingTextBox.TabIndex = 5;
            // 
            // containingCheckBox
            // 
            this.containingCheckBox.AutoSize = true;
            this.containingCheckBox.Location = new System.Drawing.Point(6, 73);
            this.containingCheckBox.Name = "containingCheckBox";
            this.containingCheckBox.Size = new System.Drawing.Size(224, 17);
            this.containingCheckBox.TabIndex = 4;
            this.containingCheckBox.Text = "Files containing the string (case sensitive):";
            this.containingCheckBox.UseVisualStyleBackColor = true;
            this.containingCheckBox.CheckedChanged += new System.EventHandler(this.containingCheckBox_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.ResourceMatchResults);
            this.groupBox2.Controls.Add(this.btnMapToDB);
            this.groupBox2.Controls.Add(this.delimeterTextBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.writeResultsButton);
            this.groupBox2.Controls.Add(this.resultsList);
            this.groupBox2.Location = new System.Drawing.Point(18, 333);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1047, 330);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Results";
            // 
            // btnMapToDB
            // 
            this.btnMapToDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMapToDB.Location = new System.Drawing.Point(891, 272);
            this.btnMapToDB.Name = "btnMapToDB";
            this.btnMapToDB.Size = new System.Drawing.Size(150, 23);
            this.btnMapToDB.TabIndex = 5;
            this.btnMapToDB.Text = "Map to DB";
            this.btnMapToDB.UseVisualStyleBackColor = true;
            this.btnMapToDB.Click += new System.EventHandler(this.btnMapToDB_Click);
            // 
            // delimeterTextBox
            // 
            this.delimeterTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.delimeterTextBox.Location = new System.Drawing.Point(254, 303);
            this.delimeterTextBox.MaxLength = 4;
            this.delimeterTextBox.Name = "delimeterTextBox";
            this.delimeterTextBox.Size = new System.Drawing.Size(38, 20);
            this.delimeterTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 306);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(249, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Delimeter for text file (may include escapes \\r,\\n,\\t):";
            // 
            // writeResultsButton
            // 
            this.writeResultsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.writeResultsButton.Location = new System.Drawing.Point(891, 301);
            this.writeResultsButton.Name = "writeResultsButton";
            this.writeResultsButton.Size = new System.Drawing.Size(150, 23);
            this.writeResultsButton.TabIndex = 3;
            this.writeResultsButton.Text = "Write results to text file...";
            this.writeResultsButton.UseVisualStyleBackColor = true;
            this.writeResultsButton.Click += new System.EventHandler(this.writeResultsButton_Click);
            // 
            // resultsList
            // 
            this.resultsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.resultsList.ContextMenuStrip = this.contextMenuStrip;
            this.resultsList.FullRowSelect = true;
            this.resultsList.HideSelection = false;
            this.resultsList.Location = new System.Drawing.Point(584, 19);
            this.resultsList.MultiSelect = false;
            this.resultsList.Name = "resultsList";
            this.resultsList.ShowItemToolTips = true;
            this.resultsList.Size = new System.Drawing.Size(451, 276);
            this.resultsList.TabIndex = 0;
            this.resultsList.UseCompatibleStateImageBehavior = false;
            this.resultsList.View = System.Windows.Forms.View.Details;
            this.resultsList.Visible = false;
            this.resultsList.DoubleClick += new System.EventHandler(this.resultsList_DoubleClick);
            this.resultsList.Resize += new System.EventHandler(this.resultsList_Resize);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Path";
            this.columnHeader1.Width = 111;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Size";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader2.Width = 90;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Last modified";
            this.columnHeader3.Width = 120;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "ResourceKeyName";
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openContainingFolderContextMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(198, 26);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // openContainingFolderContextMenuItem
            // 
            this.openContainingFolderContextMenuItem.Name = "openContainingFolderContextMenuItem";
            this.openContainingFolderContextMenuItem.Size = new System.Drawing.Size(197, 22);
            this.openContainingFolderContextMenuItem.Text = "Open containing folder";
            this.openContainingFolderContextMenuItem.Click += new System.EventHandler(this.openContainingFolderContextMenuItem_Click);
            // 
            // btnMapToDatabase
            // 
            this.btnMapToDatabase.Location = new System.Drawing.Point(0, 0);
            this.btnMapToDatabase.Name = "btnMapToDatabase";
            this.btnMapToDatabase.Size = new System.Drawing.Size(75, 23);
            this.btnMapToDatabase.TabIndex = 0;
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Location = new System.Drawing.Point(981, 274);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 8;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Visible = false;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.stopButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(900, 274);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 7;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Visible = false;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // txtCurrentResourceKey
            // 
            this.txtCurrentResourceKey.Location = new System.Drawing.Point(129, 276);
            this.txtCurrentResourceKey.Name = "txtCurrentResourceKey";
            this.txtCurrentResourceKey.Size = new System.Drawing.Size(196, 20);
            this.txtCurrentResourceKey.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 279);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Current ResourceKey";
            // 
            // comboPSprojectType
            // 
            this.comboPSprojectType.FormattingEnabled = true;
            this.comboPSprojectType.Items.AddRange(new object[] {
            "MyPS",
            "DataEntry",
            "Foodbook",
            "Webapi",
            "Login",
            "Permalink",
            "Productsheet"});
            this.comboPSprojectType.Location = new System.Drawing.Point(117, 52);
            this.comboPSprojectType.Name = "comboPSprojectType";
            this.comboPSprojectType.Size = new System.Drawing.Size(256, 21);
            this.comboPSprojectType.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "PS Project:";
            // 
            // btnAnotherSearch
            // 
            this.btnAnotherSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnotherSearch.Location = new System.Drawing.Point(909, 304);
            this.btnAnotherSearch.Name = "btnAnotherSearch";
            this.btnAnotherSearch.Size = new System.Drawing.Size(147, 23);
            this.btnAnotherSearch.TabIndex = 14;
            this.btnAnotherSearch.Text = "Start zoekopdracht";
            this.btnAnotherSearch.UseVisualStyleBackColor = true;
            this.btnAnotherSearch.Click += new System.EventHandler(this.btnAnotherSearch_Click);
            // 
            // ResourceMatchResults
            // 
            this.ResourceMatchResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResourceMatchResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.ResourceMatchResults.ContextMenuStrip = this.contextMenuStrip;
            this.ResourceMatchResults.FullRowSelect = true;
            this.ResourceMatchResults.HideSelection = false;
            this.ResourceMatchResults.Location = new System.Drawing.Point(16, 21);
            this.ResourceMatchResults.MultiSelect = false;
            this.ResourceMatchResults.Name = "ResourceMatchResults";
            this.ResourceMatchResults.ShowItemToolTips = true;
            this.ResourceMatchResults.Size = new System.Drawing.Size(562, 276);
            this.ResourceMatchResults.TabIndex = 6;
            this.ResourceMatchResults.UseCompatibleStateImageBehavior = false;
            this.ResourceMatchResults.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "ResourceID";
            this.columnHeader9.Width = 140;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Key";
            this.columnHeader10.Width = 137;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "LocationReference";
            this.columnHeader11.Width = 253;
            // 
            // MainWindow
            // 
            this.AcceptButton = this.startButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.stopButton;
            this.ClientSize = new System.Drawing.Size(1070, 675);
            this.Controls.Add(this.btnAnotherSearch);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboPSprojectType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCurrentResourceKey);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.includeSubDirsCheckBox);
            this.Controls.Add(this.selectSearchDirButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchDirTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fileNameTextBox);
            this.MinimumSize = new System.Drawing.Size(485, 490);
            this.Name = "MainWindow";
            this.Text = "File Searcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox searchDirTextBox;
        private System.Windows.Forms.Button selectSearchDirButton;
        private System.Windows.Forms.CheckBox includeSubDirsCheckBox;
        private System.Windows.Forms.CheckBox newerThanCheckBox;
        private System.Windows.Forms.DateTimePicker newerThanDateTimePicker;
        private System.Windows.Forms.DateTimePicker olderThanDateTimePicker;
        private System.Windows.Forms.CheckBox olderThanCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox containingTextBox;
        private System.Windows.Forms.CheckBox containingCheckBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView resultsList;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.RadioButton unicodeRadioButton;
        private System.Windows.Forms.RadioButton asciiRadioButton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openContainingFolderContextMenuItem;
        private System.Windows.Forms.TextBox delimeterTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button writeResultsButton;
        private System.Windows.Forms.TextBox txtCurrentResourceKey;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button btnMapToDatabase;
        private System.Windows.Forms.ComboBox comboPSprojectType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnMapToDB;
        private System.Windows.Forms.Button btnAnotherSearch;
        private System.Windows.Forms.ListView ResourceMatchResults;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
    }
}

