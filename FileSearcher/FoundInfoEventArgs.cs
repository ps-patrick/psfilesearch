﻿using System;
using System.Collections.Generic;
using System.IO;



namespace FileSearcher
{
    public class FoundInfoEventArgs
    {
        // ----- Variables -----

        private FileSystemInfo m_info;

        private string _resourcenamestring;
        // ----- Constructor -----

        public FoundInfoEventArgs(FileSystemInfo info, string ResourceKeyName)
        {
            m_info = info;
            //_resourcenamestring = "Blaatschaapje123";    
            _resourcenamestring = ResourceKeyName;
        }

        // ----- Public Properties -----

        public FileSystemInfo Info
        {
            get { return m_info; }
        }

        public string ResourceKey
        {
            get { return _resourcenamestring;  }
        }
    }
}
