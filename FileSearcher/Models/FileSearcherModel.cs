﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSearcher.Models
{
    public class FileSearcherModel
    {
        public enum MappingProjectType
        {
            MyPS = 1,
            DataEntry = 2,
            Foodbook = 3,
            Webapi = 4,
            Login = 5,
            Permalink = 6,
            Productsheet = 7
        }

        public class ApplicationResourceDto
        { 
            public int ResourceKeyID { get; set; }
            public string ResourceKey { get; set; }
        }

    }
}
