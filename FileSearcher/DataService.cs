﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using static FileSearcher.Models.FileSearcherModel;

namespace FileSearcher
{
    public class DataService
    {
        private static DataService instance;
        public static DataService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataService();
                }
                return instance;
            }
        }

        //public List<string> LoadPSResourceKeys()
        public List<ApplicationResourceDto> LoadPSResourceKeys(MappingProjectType type)
        {
            int typeID = (int)(MappingProjectType)type;

            List<ApplicationResourceDto> result = new List<ApplicationResourceDto>();
            //string query = "select top 100 AR.[Key] AS 'ResourceKey' from [master].ApplicationResource AR ";
            //string query = "select top 100 AR.[Key] AS 'ResourceKey' from [master].ApplicationResource AR WHERE [key] LIKE '%MyPS%'";
            //string query = "select DISTINCT(AR.[Key]) AS 'ResourceKey', AR.Id AS 'ResourceKeyID' from [master].ApplicationResource AR WHERE [key] LIKE '%MyPS%' AND AR.Id IN (31001,30713,31474,31473,31470,31472)";

            string query = $@" DECLARE @crmpp TABLE (projecttype int, id int)  
                              INSERT INTO @crmpp (projecttype, id)
                              SELECT DISTINCT {typeID}, (ARM.ResourceKeyID) FROM [master].ApplicationResourceMapping ARM WHERE ARM.ApplicationMappingType = {typeID} 

                              SELECT DISTINCT(AR.[Key]) AS 'ResourceKey', AR.Id AS 'ResourceKeyID' from [master].ApplicationResource AR WHERE AR.Id NOT IN (SELECT id FROM @crmpp)
                            ";
            //SELECT DISTINCT(AR.[Key]) AS 'ResourceKey', AR.Id AS 'ResourceKeyID' from [master].ApplicationResource AR WHERE [key] LIKE '%MyPS%' AND AR.Id NOT IN (SELECT id FROM @crmpp)
            using (var conn = new SqlConnection(AppSettings.Instance.ConnectionString))
            {
                result = conn.Query<ApplicationResourceDto>(query).AsList();
            }
            return result;
        }

        public void WriteProjectMappingResultRowToDB(string resourcename, MappingProjectType type, string reference, int resourceKeyID)
        {
            try
            {
                int typeID = (int)(MappingProjectType)type;
                string strippedLocationReference = "";
                if (type == MappingProjectType.DataEntry)
                {
                    strippedLocationReference = reference.Remove(0, reference.IndexOf("Ps."));
                }
                else
                {
                    strippedLocationReference = reference.Remove(0, reference.IndexOf("PS."));
                }
                

                string query = $"INSERT INTO [master].[ApplicationResourceMapping]  SELECT {typeID}, {resourceKeyID}, '{resourcename}', '{strippedLocationReference}', GETDATE(), 'Patrick', GETDATE(), 'Patrick'";
                using (var conn = new SqlConnection(AppSettings.Instance.ConnectionString))
                {
                    conn.Execute(query);

                }
            }
            catch(Exception ex)
            {

            }
            
        }
    }
}
