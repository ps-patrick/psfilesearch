﻿using System;
using System.Text;
using System.Collections.Generic;



namespace FileSearcher
{
    public class SearcherParams
    {
        // ----- Variables -----

        private string m_searchDir;
        private bool m_includeSubDirsChecked;
        private List<string> m_fileNames;
        private bool m_newerThanChecked;
        private DateTime m_newerThanDateTime;
        private bool m_olderThanChecked;
        private DateTime m_olderThanDateTime;
        private bool m_containingChecked;
        private string m_containingText;
        private string m_originalResourceKey;
        private Encoding m_encoding;

        // ----- Constructor -----

        public SearcherParams(string searchDir,
                                bool includeSubDirsChecked,
                                List<string> fileNames,
                                bool newerThanChecked,
                                DateTime newerThanDateTime,
                                bool olderThanChecked,
                                DateTime olderThanDateTime,
                                bool containingChecked,
                                string containingText,
                                string originalResourceKey,
                                Encoding encoding)
        {
            m_searchDir = searchDir;
            m_includeSubDirsChecked = includeSubDirsChecked;
            m_fileNames = fileNames;
            m_newerThanChecked = newerThanChecked;
            m_newerThanDateTime = newerThanDateTime;
            m_olderThanChecked = olderThanChecked;
            m_olderThanDateTime = olderThanDateTime;
            m_containingChecked = containingChecked;
            m_containingText = containingText;
            m_originalResourceKey = originalResourceKey;
            m_encoding = encoding;
        }


        // ----- Public Properties -----

        public string SearchDir
        {
            get { return m_searchDir; }
        }

        public bool IncludeSubDirsChecked
        {
            get { return m_includeSubDirsChecked; }
        }

        public List<string> FileNames
        {
            get { return m_fileNames; }
        }

        public bool NewerThanChecked
        {
            get { return m_newerThanChecked; }
        }

        public DateTime NewerThanDateTime
        {
            get { return m_newerThanDateTime; }
        }

        public bool OlderThanChecked
        {
            get { return m_olderThanChecked; }
        }

        public DateTime OlderThanDateTime
        {
            get { return m_olderThanDateTime; }
        }

        public bool ContainingChecked
        {
            get { return m_containingChecked; }
        }

        public string ContainingText
        {
            get { return m_containingText; }
        }

        public string OriginalResourceKey
        {
            get { return m_originalResourceKey; }
        }

        public Encoding Encoding
        {
            get { return m_encoding; }
        }
    }
}
